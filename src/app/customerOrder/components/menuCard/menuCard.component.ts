import { CreateOrderComponent } from './../../createOrder/createOrder.component';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menuCard',
  templateUrl: './menuCard.component.html',
  styleUrls: ['./menuCard.component.css']
})
export class MenuCardComponent implements OnInit {
  @Input() menu: CreateOrderComponent;
  myimg = 'assets/imgPlaceholder.png';
  
  constructor() { }

  ngOnInit() {
    
  }
  
  createOrder(el) {
    let itemPrice = el;
    console.log("Product Price: ", itemPrice);
  }

}
