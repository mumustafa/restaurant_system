import { MenuService } from './../../services/menu.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-createOrder',
  templateUrl: './createOrder.component.html',
  styleUrls: ['./createOrder.component.css']
})
export class CreateOrderComponent implements OnInit {
  // order = [];
  // menu = [];
  // total = 0;
  menuItems: any[] = [];
  category: string;
  filteredProducts: any[] = [];
  
  constructor(
    public route: ActivatedRoute, 
    private _menuService: MenuService
    ) { }

  ngOnInit() {
    this._menuService.getMenu().subscribe((data: any) => {
      this.menuItems = data.items;
    })
    this.route.queryParamMap.subscribe((data: any) => {
      this.category = data.get('category');

      this.filteredProducts = (this.category) ?
        this.menuItems.filter(m => m.category === this.category) :
        this.menuItems;
    })
  }
  
  addItem(id){
    // if item in order { increment quantity }
    // else { push item in order }
  }

  removeItem(id){
    // if item in order { decrement quantity }
    // else { remove item from order }
  }

}
