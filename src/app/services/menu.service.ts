import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private itemsInCartSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private itemsInCart: any[];
  apiUrl = environment.apiUrl;
  constructor(private _http: HttpClient) { }

  public getMenu() {
    return this._http.get(this.apiUrl + '/menu/list/api/');
  }
  public addToCart(item: any[]) {
    this.itemsInCartSubject.next([...this.itemsInCart, item]);
  }

  public getItems(): Observable<any[]> {
    return this.itemsInCartSubject;
  }
}
