import { BillingOrderComponent } from './customerOrder/billingOrder/billingOrder.component';
import { CreateOrderComponent } from './customerOrder/createOrder/createOrder.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MenuCardComponent } from './customerOrder/components/menuCard/menuCard.component';


@NgModule({
   declarations: [
      AppComponent,
      CreateOrderComponent,
      MenuCardComponent,
      BillingOrderComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      RouterModule.forRoot([
         { path: '', component:  AppComponent},
         { path: 'order', redirectTo: 'order/create' },
         { path: 'order/create', component: CreateOrderComponent },
      ])
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
